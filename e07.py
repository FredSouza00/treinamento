# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'



def ex01():
    """Faça um Programa que peça dois números e imprima o maior deles.
"""
    numeros = []
    for i in range(2):
        numeros.append(int(input(f"Informe o {i}º numero inteiro diferentes e descubra o maior: ")))

    print(f"{(('Os numeros são iguais') if numeros[0] == numeros[1] else (f'O numero {numeros[0]} é o maior')) if numeros[0] >= numeros[1] else (f'O numero {numeros[1]} é o maior')}")

def ex02():
    """Faça um Programa que peça um valor e mostre na tela se o valor é positivo ou negativo"""
    numeros = int(input(f"Informe o numero positivo ou negativo: "))
    print(f"{(('Ficou curioso né? O zero é o unico dos numeros  não positivo nem negativo.') if numeros == 0 else (f'O numero {numeros} é positivo')) if numeros >= 0 else (f'O numero {numeros} negativo')}")

def ex03():
    """Faça um Programa que verifique se uma letra digitada é "F" ou "M". Conforme a letra escrever: F - Feminino, M - Masculino, Sexo Inválido.
"""
    dict = {'f':'Feminino','m': 'masculino'}
    letra = input(f"Informe seu sexo usando  'F' para feminino ou 'M' para masculino :")
    print(f"Seu sexo é : {dict.get(letra.lower())}")
def ex04():
    """Faça um Programa que verifique se uma letra digitada é vogal ou consoante.
"""
    vogal = "aeiou"
    letra = input("Informe uma letra e descubra se é vogal:")
    print(f"A letra: {letra} é uma vogal" if letra.lower() in vogal else f"A letra :{letra}  é uma consoante")

def ex05():
    """Faça um programa para a leitura de duas notas parciais de um aluno. O programa deve calcular a média alcançada por aluno e apresentar:
A mensagem "Aprovado", se a média alcançada for maior ou igual a sete;
A mensagem "Reprovado", se a média for menor do que sete;
A mensagem "Aprovado com Distinção", se a média for igual a dez
"""
    notas= []
    for i in range(2):
        notas.append(float(input(f"Informe a {i+1}º nota do aluno:")))
    media = (notas[0] + notas[1]) / len(notas)
    print((f"Aprovado com Distinção com a média {media}" if media == 10 else f"Aprovado com média {media}" )if media >= 7 else f"Reprovado com a média {media}")

def ex06():
    """Faça um Programa que leia três números e mostre o maior deles.
"""
    numeros = []
    for i in range(3):
        numeros.append(float(input(f"Informe o {i+1}º numero de 3 e decubra qual é o maior:")))
    print(f"O maior numero é o {max(numeros)}!!")

def ex07():
    """Faça um Programa que leia três números e mostre o maior e o menor deles"""
    numeros = []
    for i in range(3):
        numeros.append(float(input(f"Informe o {i + 1}º numero de 3 e decubra qual é o maior e o menor:")))
    print(f"O maior numero é o {max(numeros)}e o menor é o {min(numeros)}!!")
def ex08():
    """Faça um programa que pergunte o preço de três produtos e informe qual produto você deve comprar, sabendo que a decisão é sempre pelo mais barato.
"""
    precos = []
    for i in range(3):
        precos.append(float(input(f"Informe o preço do {i + 1}º item  de 3:")))
    print(f"O produto mais em conta é o {precos.index(min(precos))+1}º informado,  de preço {min(precos)}.")

def ex09():
    """Faça um Programa que leia três números e mostre-os em ordem decrescente."""
    numeros = []
    for i in range (3):
        numeros.append(int(input(f"Informe o {i+1}º de 3 numeros e veja sua ordem decrescente: ")))
    print(sorted(numeros,reverse=True))

def ex10():
    """Faça um Programa que pergunte em que turno você estuda. Peça para digitar M-matutino ou V-Vespertino ou N- Noturno. Imprima a mensagem "Bom Dia!", "Boa Tarde!" ou "Boa Noite!" ou "Valor Inválido!", conforme o caso.
"""
    greatings_dict = {'matutino':'Bom dia!!!','vespertino':'Boa tarde!!!','noturno':'Boa noite!!!'}
    turno_dict = {'m':'matutino','v':'vespertino','n':'noturno'}
    aux = input("Informe o seu turno de estudo:\n----legenda-----\n[M] para matutino\n[V] para Vespertino\n[N] para Noturno: ")
    if aux.lower() in turno_dict.keys():
        print(f"Você estuda no periodo da {turno_dict.get(aux.lower())}, {greatings_dict.get(turno_dict.get(aux.lower()))}")
    else:
        print("Informação inválida")

def ex11():
    """As Organizações Tabajara resolveram dar um aumento de salário aos seus colaboradores e lhe contrataram para desenvolver o programa que calculará os reajustes.
Faça um programa que recebe o salário de um colaborador e o reajuste segundo o seguinte critério, baseado no salário atual:
salários até R$ 280,00 (incluindo) : aumento de 20%
salários entre R$ 280,00 e R$ 700,00 : aumento de 15%
salários entre R$ 700,00 e R$ 1500,00 : aumento de 10%
salários de R$ 1500,00 em diante : aumento de 5% Após o aumento ser realizado, informe na tela:
o salário antes do reajuste;
o percentual de aumento aplicado;
o valor do aumento;
o novo salário, após o aumento.
"""
    salarios_categoria={280:1,281:2,700:2,701:3,1500:3,1501:4}
    categoria_reajuste={1:0.2,2:0.15,3:0.1,4:0.05}
    salario = float(input(f"Informe seu salário atual: "))
    if salario <= 280:
        print(f"Seu salário de: {salario:.2f} terá aumento de {categoria_reajuste.get(salarios_categoria.get(280))*100}"
              f"%  seu novo salário é {(salario*categoria_reajuste.get(salarios_categoria.get(280))+salario):.2f}")
        return
    elif  salario <= 700:
        print(f"Seu salário de: {salario:.2f} terá aumento de {categoria_reajuste.get(salarios_categoria.get(700))*100}"
              f"%  seu novo salário é {(salario * categoria_reajuste.get(salarios_categoria.get(700)) + salario):.2f}")
        return
    elif salario <= 1500:
        print(f"Seu salário de: {salario:.2f} terá aumento de {categoria_reajuste.get(salarios_categoria.get(1500))*100}"
              f"%  seu novo salário é {(salario * categoria_reajuste.get(salarios_categoria.get(1500)) + salario):.2f}")
        return
    else:
        print(f"Seu salário de: {salario:.2f} terá aumento de {categoria_reajuste.get(salarios_categoria.get(1501))*100}"
              f"%  seu novo salário é {(salario * categoria_reajuste.get(salarios_categoria.get(1501)) + salario):.2f}")


def ex12():
    """Faça um programa para o cálculo de uma folha de pagamento, sabendo que os descontos são do Imposto de Renda, que
    depende do salário bruto (conforme tabela abaixo) e 3% para o Sindicato e que o FGTS corresponde a 11% do Salário
    Bruto, mas não é descontado (é a empresa que deposita). O Salário Líquido corresponde ao Salário Bruto menos os descontos. O programa deverá pedir ao usuário o valor da sua hora e a quantidade de horas trabalhadas no mês.
Desconto do IR:
Salário Bruto até 900 (inclusive) - isento
Salário Bruto até 1500 (inclusive) - desconto de 5%
Salário Bruto até 2500 (inclusive) - desconto de 10%
Salário Bruto acima de 2500 - desconto de 20% Imprima na tela as informações, dispostas conforme o exemplo abaixo. No exemplo o valor da hora é 5 e a quantidade de hora é 220.
        Salário Bruto: (5 * 220)        : R$ 1100,00
        (-) IR (5%)                     : R$   55,00
        (-) INSS ( 10%)                 : R$  110,00
        FGTS (11%)                      : R$  121,00
        Total de descontos              : R$  165,00
        Salário Liquido                 : R$  935,00
"""



    horas_mensais = float(input(f"Informe a quantidade de horas mensais trabalhadas:"))
    salario_hora = float(input(f"Informe  o preço de sua hora de trabalho:"))
    salario_bruto = horas_mensais*salario_hora
    descontos_dict = {'sindicato': 0.03,'fgts':0.11}
    salario_desconto_ir ={900:0,1500:0.05,2500:0.1,2501:0.2}
    desconto_ir_ponteiro = 0
    fgts = (descontos_dict.get('fgts')*100)
    sindicato =(descontos_dict.get('sindicato')*100)


    if salario_bruto <= 900:
        desconto_ir_ponteiro = 900
    elif (salario_bruto >900) & (salario_bruto<= 1500):
        desconto_ir_ponteiro = 1500
    elif (salario_bruto > 1500) & (salario_bruto <=2500):
        desconto_ir_ponteiro = 2500
    else:
        desconto_ir_ponteiro = 2501
    print("___________________TABELA___________________")
    print(f"Salário Bruto               :R${ salario_bruto}\n(-) IR({(salario_desconto_ir[desconto_ir_ponteiro]*100)}%)"
          f"                :R$ {salario_bruto*(salario_desconto_ir[desconto_ir_ponteiro])}\n"
          f"(-)SINDICATO({sindicato})%          :R${(salario_bruto*descontos_dict.get('sindicato'))}\n"
          f"FGTS({fgts}%)                 :R${salario_bruto*descontos_dict.get('fgts')}\n"
          f"Total de descontos:         :R${(salario_bruto * descontos_dict.get('sindicato'))+(salario_bruto*(salario_desconto_ir[desconto_ir_ponteiro]))}")

if __name__ == "__main__":
    ex01()
    ex02()
    ex03()
    ex04()
    ex05()
    ex06()
    ex07()
    ex08()
    ex09()
    ex10()
    ex11()
    ex12()
