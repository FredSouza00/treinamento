# coding: iso-8859-1 -*-
# 11 Fa�a um Programa que pe�a 2 n�meros inteiros e um n�mero real. Calcule e mostre:
  # o produto do dobro do primeiro com metade do segundo .
  # a soma do triplo do primeiro com o terceiro.
  # o terceiro elevado ao cubo.


  
numeroInteiro1 = int(input("Informe o valor do primeiro numero inteiro: "))
numeroInteiro2 = int(input("Informe o valor do segundo numero inteiro: "))
numeroReal = float(input("Insira um n�mero real: "))

#o produto do dobro do primeiro com metade do segundo
operacao1 = (numeroInteiro1 * 2) * (numeroInteiro2 / 2)


#a soma do triplo do primeiro com o terceiro.
operacao2 = (numeroInteiro1 * 3) + numeroReal

#o terceiro elevado ao cubo
operacao3 = numeroReal ** 3



print(f"O produto do dobro do primeiro com metade do segundo: [{operacao1}]")
print(f"A soma do triplo do primeiro com o terceiro: [{operacao2}]")
print(f'O terceiro elevado ao cubo �: [{operacao3:.02f}]')