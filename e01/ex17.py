# coding: iso-8859-1 -*-
#17 Fa�a um Programa para uma loja de tintas. O programa dever� pedir o tamanho em metros quadrados da �rea a ser pintada. Considere que a cobertura da tinta � de 1 litro para cada 6 metros quadrados e que a tinta � vendida em latas de 18 litros, que custam R$ 80,00 ou em gal�es de 3,6 litros, que custam R$ 25,00.
#Informe ao usu�rio as quantidades de tinta a serem compradas e os respectivos pre�os em 3 situa��es:
#comprar apenas latas de 18 litros;
#comprar apenas gal�es de 3,6 litros;
#Misture latas e gal�es, de forma que o desperd�cio de tinta seja menor. Acrescente 10% de folga e sempre arredonde os valores para cima, isto �, considere latas cheias.


metrosPintura= float(input("Informe o tamanho em m� da �rea a ser pintada: "))
litrosTintaNecessario = metrosPintura/6
latasTintaNecessario = litrosTintaNecessario/18
galoesTintaNecessario = litrosTintaNecessario/3.6



aviso=""
latasComprar= 0
while latasTintaNecessario >= 0:
  if (litrosTintaNecessario < 18)&(latasComprar==0):
      aviso="#######ALERTA:Quantidade inferior a uma lata de tinta.##########\n Reconsidere comprar uma lata menor"  
  latasTintaNecessario -= 1
  latasComprar += 1

orcamentoTintaLata=latasComprar*80

print("--------------Or�amento da compra em latas--------------------------------------")
print(f"Para pintar a �rea de [{metrosPintura}m�], s�o necess�rias [{latasComprar} latas]de tinta de 18 lts que podem ser compradas no valor de [{orcamentoTintaLata:.02f}R$] \n {aviso}")

aviso=""
galoesComprar=0
while galoesTintaNecessario >= 0:
  if (litrosTintaNecessario < 3.6)&(galoesComprar==0):
      aviso="#######ALERTA:Quantidade inferior a um gal�o de tinta.##########\n Reconsidere comprar uma gal�o menor"  
  galoesTintaNecessario -=1
  galoesComprar += 1

orcamentoTintagalao=galoesComprar*25
print("--------------Or�amento da compra em gal�es--------------------------------------")
print(f"Para pintar a �rea de [{metrosPintura}m�], s�o necess�rias [{galoesComprar} gal�es]de tinta de 3.6 lts que podem ser compradas no valor de [{orcamentoTintagalao:.02f}R$] \n {aviso}")

galoesComprar=0
latasComprar= 0
while litrosTintaNecessario > 0:
  if litrosTintaNecessario < 18:
      litrosTintaNecessario -=3.6
      galoesComprar+=1
      continue
  litrosTintaNecessario -=18
  latasComprar += 1

orcamentoTintagalao=galoesComprar*25
orcamentoTintaLata=latasComprar*80

print("--------------Or�amento da compra Mista--------------------------------------")
print(f"Para pintar a �rea de [{metrosPintura}m�], s�o necess�rias [{latasComprar} latas]de tinta de 18 lts e [{galoesComprar} galoes] de tinta de 3.6 lts  que podem ser compradas no valor de [{orcamentoTintagalao+orcamentoTintaLata:.02f}R$] \n {aviso}")






