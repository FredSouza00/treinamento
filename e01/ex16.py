# coding: iso-8859-1 -*-
#16 Fa�a um programa para uma loja de tintas. O programa dever� pedir o tamanho em metros quadrados da �rea a ser pintada. Considere que a cobertura da tinta � de 1 litro para cada 3 metros quadrados e que a tinta � vendida em latas de 18 litros, que custam R$ 80,00. Informe ao usu�rio a quantidade de latas de tinta a serem compradas e o pre�o total.



metrosPintura= float(input("Informe o tamanho em m� da �rea a ser pintada: "))
litrosTintaNecessario= metrosPintura/3
latasTintaNecessario = litrosTintaNecessario/18
aviso=""
latasComprar= 0


while latasTintaNecessario >= 0:
  if (litrosTintaNecessario < 18)&(latasComprar==0):
      aviso="#######ALERTA:Quantidade inferior a uma lata de tinta.##########\n Reconsidere comprar uma lata menor"  
  latasTintaNecessario -= 18
  latasComprar += 1
orcamentoTintaLata=latasComprar*80
print("--------------Or�amento da compra em latas--------------------------------------")
print(f"Para pintar a �rea de [{metrosPintura}m�], s�o necess�rias [{latasComprar} latas]de tinta de 18 lts que podem ser compradas no valor de [{orcamentoTintaLata:.02f}R$] \n {aviso}")
print("---------------------------------------------------------------------------------")