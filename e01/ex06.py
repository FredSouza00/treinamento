# coding: iso-8859-1 -*-
#06 Fa�a um Programa que pe�a o raio de um c�rculo, calcule e mostre sua �rea e per�metro.

raio = float(input("Insira o valor do Raio para calcular a �rea e per�metro de um c�rculo: "))
pi= 3.14
perimetro = 2 * pi * raio
area = pi * (raio ** 2)
print(f"Um c�rculo com raio de {raio:.02f}, possui per�metro de:{perimetro:.02f}e �rea de {area:.02f}")