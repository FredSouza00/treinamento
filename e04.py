# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'

def ex01():
#Faça um programa que peça um número inteiro e determine se ele é ou não um número primo. Um número primo é aquele que é divisível somente por ele mesmo e por 1.
    numero = int(input(f"Informe  um numero inteiro  e  verifique se ele é um numero primo"))
    qtdMultiplos = 0
    contador = 1
    while contador <= numero:
        if (numero % contador != 0)&(contador != 1):
            contador += 1
            continue
        qtdMultiplos += 1
        contador+= 1
    if qtdMultiplos != 2:
        print(f"O numero {numero} não é numero primo!!!")
    else:
        print(f"O numero {numero} é um numero primo!!!")

def ex02():
#Copie e altere o programa de cálculo dos números primos, informando, caso o número não seja primo, por quais número ele é divisível.

    numero = int(input(f"Informe  um numero inteiro  e  verifique se ele é um numero primo  e se não for quais são os numeros os quais ele é divisível "))
    qtdMultiplos = 0
    contador = 1
    listaDivisiveis= []
    while contador <= numero:
        if (numero % contador != 0)&(contador != 1):

            contador += 1
            continue
        listaDivisiveis.append(contador)
        qtdMultiplos += 1
        contador+= 1

    if qtdMultiplos != 2:

        print(f"O numero {numero} não é numero primo!!! e seus divisíveis são {listaDivisiveis}")
    else:
        print(f"O numero {numero} é um numero primo!!!")

def ex03():
#Faça um programa que mostre todos os primos entre 1 e N sendo N um número inteiro fornecido pelo usuário. O programa deverá mostrar também o número de divisões que ele executou para encontrar os números primos. Serão avaliados o funcionamento, o estilo e o número de testes (divisões) executados.
    numero = int(input("Informe um número para verificar os números primos entre ele e 1: "))
    lista = []
    contadorOperacoes = 0
    for i in range(numero +1):
        contador = 1
        qtdMultiplos = 0
        while contador <= i:
            contadorOperacoes += 1
            if (i % contador != 0) & (contador != 1):
                contador += 1
                continue
            qtdMultiplos += 1
            contador += 1

        if qtdMultiplos != 2:
            print(f"O numero {i} não é numero primo!!!")
        else:
            lista.append(i)
            print(f"O numero {i} é um numero primo!!!")

    print(f"Números primos: {lista} e fora feitas {contadorOperacoes} divisões")


def ex04():
#Faça um programa que leia uma quantidade indeterminada de números positivos e conte quantos deles estão nos seguintes intervalos: [0-25], [26-50], [51-75] e [76-100]. A entrada de dados deverá terminar quando for lido um número negativo.
    numero = 0
    intervalo025 = []
    intervalo2650 = []
    intervalo5175 = []
    intervalo76100 = []
    while (numero >= 0)& (numero<=100):

        if numero <= 25:
            intervalo025.append(numero)
            numero = int(input("Informe o numero para ser classificado  em intervalos:"))
            continue
        elif numero <=50:
            intervalo2650.append(numero)
            numero = int(input("Informe o numero para ser classificado  em intervalos:"))
            continue
        elif numero <=75:
            intervalo5175.append(numero)
            numero = int(input("Informe o numero para ser classificado  em intervalos:"))
            continue
        else:
            intervalo76100.append(numero)
            numero = int(input("Informe o numero para ser classificado  em intervalos:"))
            continue
    print(f'--------------intervalos---------------\n0  a 25{intervalo025}\n26 a 50{intervalo2650}\n51 a 75{intervalo5175}\n76 a 100{intervalo76100}')

def ex05():
#Faça um programa que peça para n pessoas a sua idade, ao final o programa deverá verificar se a média de idade da turma varia entre 0 e 25, 26 e 60 e maior que 60; e então, dizer se a turma é jovem, adulta ou idosa, conforme a média calculada
    numeroPessoas = int(input("Informe a quantidade de alunos na turma: "))
    intervalo025 = []
    intervalo2650 = []
    intervalo60 = []
    for i in range(numeroPessoas):
        idade = int(input(f"Informe a idade da {i}º pessoa:"))
        if idade <= 25:
            intervalo025.append(idade)
            continue
        elif idade <=50:
            intervalo2650.append(idade)
            continue
        else:
            intervalo60.append(idade)
            continue
    if (len(intervalo2650)>=len(intervalo025) )& (len(intervalo2650)>=len(intervalo60)):
        print("É uma turma com a maioria dos alunos na idade Adulta")

    elif len(intervalo025)>= len(intervalo60):
        print("É uma turma com a maioria dos alunos na idade jovem")

    else:
        print("É uma turma com a maioria dos alunos na idade idosa")



def ex06():
#Numa eleição existem três candidatos. Faça um programa que peça o número total de eleitores. Peça para cada eleitor votar e ao final mostrar o número de votos de cada candidato.
    eleitores= int(input("Informe a quantidade total de eleitores:"))
    candidato1 = []
    candidato2 = []
    candidato3 = []
    for i in range(eleitores +1):
        voto = input("Encontre o numero do seu candidato\n Candidato Um Da Silva : nº [18]\n Candidato Dois De Paula : nº [28]\n Candidato Três Dos Santos : nº [38]\n Informe o numero do seu candidato e aperte ENTER")
        if voto == "18":
            candidato1.append(i)
            print(f"Voto completa com sucesso")
            continue
        elif voto == "28":
            candidato2.append(i)
            print(f"Voto completa com sucesso")
            continue
        elif voto == "38":
            candidato3.append(i)
            print(f"Voto completa com sucesso")
            continue
        else:
            print("Voto nulo.")
    print(f"Candidatos e seus respectivos votos:\nCandidato Um Da Silva tem um total de votos de:{len(candidato1)}.\nCandidato Dois De Paula tem um total de  votos de: {len(candidato2)}.\nCandidato Três Dos Santos tem um total de votos de: {len(candidato3)}.")


def ex07():
#Faça um programa que calcule o número médio de alunos por turma. Para isto, peça a quantidade de turmas e a quantidade de alunos para cada turma. As turmas não podem ter mais de 40 alunos.
    turmas = int(input("informe a quantidade de turmas"))
    alunos_qtd = []
    contador = 1
    for contador in range(turmas):
        alunos = int(input(f"informe a quantidade da turma {contador} OBS: a turma não pode ter mais de 40 alunos"))
        if alunos <= 40:
            alunos_qtd.append(alunos)
            continue
        else:
            print("Deve conter menos de 40 alunos")
            continue
    print(f'A media da turma é {sum(alunos_qtd)/len(alunos_qtd)} alunos')

def ex08():
#Faça um programa que calcule o valor total investido por um colecionador em sua coleção de CDs e o valor médio gasto em cada um deles. O usuário deverá informar a quantidade de CDs e o valor para em cada um.
    cds_qtd = int(input("informe a quantidade de cd's comprados"))
    cds = []
    contador = 1
    for contador in range(cds_qtd):
        preco_cd = float(input(f"informe o preço do {contador} CD"))
        cds.append(preco_cd)


    print(f'O valor total investido na coleção de CDs é de {sum(cds):.2f}R$ e a média  individual de preços é de: {sum(cds)/len(cds):.2f} ')

#def ex09():
#O Sr. Manoel Joaquim possui uma grande loja de artigos de R$ 1,99, com cerca de 10 caixas. Para agilizar o cálculo de quanto cada cliente deve pagar ele desenvolveu um tabela que contém o número de itens que o cliente comprou e ao lado o valor da conta. Desta forma a atendente do caixa precisa apenas contar quantos itens o cliente está levando e olhar na tabela de preços. Você foi contratado para desenvolver o programa que monta esta tabela de preços, que conterá os preços de 1 até 50 produtos, conforme o exemplo abaixo:





if __name__ == "__main__":
    ex01()
    ex02()
    ex03()
    ex04()
    ex05()
    ex06()
    ex07()
    ex08()