# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'

import random
from datetime import datetime

def ex01():
    """Faça um programa para imprimir:
   1
    2   2
    3   3   3
    .....
    n   n   n   n   n   n  ... n
para um n informado pelo usuário. Use uma função que receba um valor n inteiro e imprima até a n-ésima linha.
"""
    def repetir_numero(numero:int):
        for i in range(numero):
            i += 1
            print(f'{(str(i)*i)}')

    numero = int(input('Digite um número: '))
    repetir_numero(numero)




def ex02():
    """Faça um programa para imprimir:
   1
    2   2
    3   3   3
    .....
    n   n   n   n   n   n  ... n
para um n informado pelo usuário. Use uma função que receba um valor n inteiro e imprima até a n-ésima linha.
"""
    def repetir_numero(numero:int):
        for i in range(numero):
            i += 1
            print(f'{(str(i)*i)}')

    numero = int(input('Digite um número: '))
    repetir_numero(numero)

def ex03():
    """Faça um programa, com uma função que necessite de três argumentos, e que forneça a soma desses três argumentos.
"""
    numeros= []
    def fun_soma(num1:int,num2:int,num3:int):
        return num1+num2+num3
    for i in range (3):
        numeros.append(int(input(f"Informe o {i+1}ª numero de 3 e descubra a soma dos 3: ")))
    print(f"A soma  é {numeros[0]}+{numeros[1]}+{numeros[2]} = {fun_soma(numeros[0],numeros[1],numeros[2])}")

def ex04():
    """Faça um programa, com uma função que necessite de um argumento. A função retorna o valor de caractere ‘P’, se seu argumento for positivo, e ‘N’, se seu argumento for zero ou negativo.
"""

    def verificador(status:bool):
        return ("P" if status else "N")

    numero =int(input(f'Informe um numero para verificar se é positivo ou negativo:'))
    aux = (True if numero >= 0 else False)
    print(f"P= positivo | N= Negativo\nO numero:{numero} é {verificador(aux)}")


def ex05():
    """Faça um programa com uma função chamada soma_imposto. A função possui dois parâmetros formais: taxa_imposto, que é a quantia de imposto sobre vendas expressa em porcentagem e custo, que é o custo de um item antes do imposto. A função “altera” o valor de custo para incluir o imposto sobre vendas.
"""
    def taxa_imposto(imposto,custo):
        return custo+(custo*(imposto/100))

    imposto = float(input(f"Informe o imposto do produto em %"))
    custo = float(input(f"Informe o custo deste produto"))
    print(f'O preço imbuto  de impostos fica: {taxa_imposto(imposto,custo)}R$')



def ex06():
    """Faça um programa que converta da notação de 24 horas para a notação de 12 horas. Por exemplo, o programa deve converter 14:25 em 2:25 P.M. A entrada é dada em dois inteiros. Deve haver pelo menos duas funções: uma para fazer a conversão e uma para a saída. Registre a informação A.M./P.M. como um valor ‘A’ para A.M. e ‘P’ para P.M. Assim, a função para efetuar as conversões terá um parâmetro formal para registrar se é A.M. ou P.M. Inclua um loop que permita que o usuário repita esse cálculo para novos valores de entrada todas as vezes que desejar"""

    def converter_horas(horas,minutos):
        return print(f"A hora[{horas}:{minutos}] em seu novo formato ficará:[{horas%12}:{minutos}]")
    print("Siga as instruções para converter horas do formato 24 para formato 12")
    horas = int(input("Informe as horas:"))
    minutos = int(input("Informe os minutos"))
    print(converter_horas(horas,minutos))


def ex07():
    """Faça um programa que use a função valorPagamento para determinar o valor a ser pago por uma prestação de uma conta. O programa deverá solicitar ao usuário o valor da prestação e o número de dias em atraso e passar estes valores para a função valorPagamento, que calculará o valor a ser pago e devolverá este valor ao programa que a chamou. O programa deverá então exibir o valor a ser pago na tela. Após a execução o programa deverá voltar a pedir outro valor de prestação e assim continuar até que seja informado um valor igual a zero para a prestação. Neste momento o programa deverá ser encerrado, exibindo o relatório do dia, que conterá a quantidade e o valor total de prestações pagas no dia. O cálculo do valor a ser pago é feito da seguinte forma. Para pagamentos sem atraso, cobrar o valor da prestação. Quando houver atraso, cobrar 3% de multa, mais 0,1% de juros por dia de atraso.
"""
    def valor_pagamento(valor_prestacao,n_dias_atraso):
        valor_reajustado = valor_prestacao+((valor_prestacao*0.001)*n_dias_atraso)*0.03
        return valor_reajustado

    controle = 1
    while controle != 0:
        print("-----------------Para sair digite 0---------------------")
        prestacao= float(input(f"Informe o valor da parcela em atraso:"))
        if prestacao == 0:
            print("FIM")
            return
        dias = int(input(f"Informe a quantidade de dias em atraso:"))
        if dias == 0:
            print("FIM")
            return
        print(f'O valor de sua conta com juros é: \n{valor_pagamento(prestacao,dias):.2f}R$')

def ex08():
    """Faça uma função que informe a quantidade de dígitos de um determinado número inteiro informado"""

    def contar_letras(palavra:str):
        return len(palavra)

    palavra_testada = input(f"Informe um numero e descubra quantos digitos ele tem: ")
    print(f"O numero: {palavra_testada} tem:  {contar_letras(palavra_testada)} digitos")

def ex09():
    """Reverso do número. Faça uma função que retorne o reverso de um número inteiro informado. Por exemplo: 127 -> 721."""

    def num_reverso(numero:str):
       return numero[::-1]
    numero_testado = str(input(f"Informe um numero e veja ele ao contrário:"))
    print(f"O numero: {numero_testado} ao contrario fica: {num_reverso(numero_testado)}")

def ex10():
    """Jogo de Craps. Faça um programa de implemente um jogo de Craps. O jogador lança um par de dados, obtendo um valor entre 2 e 12. Se, na primeira jogada, você tirar 7 ou 11, você um "natural" e ganhou. Se você tirar 2, 3 ou 12 na primeira jogada, isto é chamado de "craps" e você perdeu. Se, na primeira jogada, você fez um 4, 5, 6, 8, 9 ou 10,este é seu "Ponto". Seu objetivo agora é continuar jogando os dados até tirar este número novamente. Você perde, no entanto, se tirar um 7 antes de tirar este Ponto novamente.
"""
    numeros_bons = [ 7 , 11]
    numeros_ruins= [2,3,12]
    retorno_bom = ['GANHOU!!!']
    retorno_ruim = ['CRAPS!!!','PERDEU!!!']

    def rolar_dados():
        dado1=random.randrange(1,7)
        dado2=random.randrange(1,7)
        print(f"Dado nº1: [{dado1}],dado nº2 [{dado2}] soma = {dado1 + dado2}")
        return dado1+dado2
    def avaliar_rodada(rodada):
        soma = rolar_dados()
        if rodada == 0:
            if soma in numeros_bons :
                return 'GANHOU!!!'
            if soma in numeros_ruins:
                return 'CRAPS!!!'
            else:
                return 'SEGUE!!!'
        else:
            if soma in numeros_bons:
                return 'GANHOU!!!'
            if soma  == 7:
                return 'PERDEU!!!'
            else:
                return 'SEGUE!!!'


    rodada = 0
    perdeu = False
    ganhou = False
    while ganhou == perdeu :
        pergunta = input(f"----------------Rodada nª[{rodada}]---------- \nDeseja jogar os dados novamente?(SIM/NÃO):")
        resultado  = ( avaliar_rodada(rodada)if pergunta.lower() == 'sim' else 'DESISTIU!!!')
        rodada +=1
        if resultado != 'SEGUE!!!':
            if resultado == 'DESISTIU!!!':
                print("Você desistiu!!! ARREGÃOO!!!")
                return
            if resultado in retorno_bom:
                print("PARABÉNS VOCÊ GANHOU O JOGO!!!")
                return
            if resultado in retorno_ruim:
                if resultado == 'CRAPS!!!':
                    print("-------------------CRAAPPPPPPPPP!!!!!-----------------\nMEU DEUS COMO VOCÊ É RUIM PERDEU NA PRIMEIRA RODADA!!!!")
                    return
                if resultado == 'PERDEU!!!':
                    print("SINTO MUITO VOCÊ PERDEU, MAS PELO MENOS NÃO FOI UM CRAP")
                    return
def ex11():
    """Data com mês por extenso. Construa uma função que receba uma data no formato DD/MM/AAAA e devolva uma string no formato D de mesPorExtenso de AAAA. Opcionalmente, valide a data e retorne NULL caso a data seja inválida."""

    mes_dict = {'01':'Janeiro','02':'Fevereiro','03':'Março','04':'Abril','05':'Maio','06':'Junho','07':'Julho','08':'Agosto','09':'Setembro','10':'Outubro','11':' Novembro','12':'Dezembro'}
    caracters_validos=['0123456789/']
    def data(dia,mes,ano):
        print(f"Sua data no formato atual é {dia}/{mes}/{ano}, seu novo formato é {dia} de {mes_dict.get(mes)} de {ano}")

    print("----------formato DD/MM/AAAA---use/--------")
    data_completa = input("Informe a data seguindo o formato formato DD/MM/AAAA:  ")
    try:
        data_completa != datetime.strftime(datetime.strptime(data_completa,"%d/%m/%Y"),"%d/%m/%Y")

    except:
        raise print(f"{data_completa} em formato incorreto siga o formato DD/MM/AAAA")

    data_processada = data_completa.split("/",3)
    data(data_processada[0],data_processada[1],data_processada[2])

def ex12():
    """Embaralha palavra. Construa uma função que receba uma string como parâmetro e devolva outra string com os carateres embaralhados. Por exemplo: se função receber a palavra python, pode retornar npthyo, ophtyn ou qualquer outra combinação possível, de forma aleatória. Padronize em sua função que todos os caracteres serão devolvidos em caixa alta ou caixa baixa, independentemente de como foram digitados.
"""
    def embaralhar_palavra(palavra):
        array = list(palavra)
        random.shuffle(array)
        array = ''.join(array)
        print(f'A palavra  normal é : {palavra:} e ficou :{array.upper()}')

    palavra = input(f"Informe uma palavra veja como fica embaralhada:")
    embaralhar_palavra(palavra)


def ex13():
    def desenha(altura,largura):
        if largura > 20:
            largura = 20
        if altura > 20:
            altura = 20
        linha = ('+'+ '-' * (largura)+'+')
        coluna = ('|'+ ('.'*largura) +'|')
        print(linha)
        for i in range(altura):
            print(coluna)
        print(linha)
    print("-----------------Desenhando---------------------")
    altura = int(input(f"Informe a altura do seu retângulo:"))
    largura = int(input(f"Informe a largura do seu retângulo:"))
    desenha(altura,largura)

def ex14():
    """Quadrado mágico. Um quadrado mágico é aquele dividido em linhas e colunas, com um número em cada posição e no qual a soma das linhas, colunas e diagonais é a mesma. Por exemplo, veja um quadrado mágico de lado 3, com números de 1 a 9:
8  3  4
1  5  9
6  7  2
Elabore uma função que identifica e mostra na tela todos os quadrados mágicos com as características acima. Dica: produza todas as combinações possíveis e verifique a soma quando completar cada quadrado. Usar um vetor de 1 a 9 parece ser mais simples que usar uma matriz 3x3.
"""
    matriz = [[8, 3, 4], [1, 5, 9], [6, 7, 2]]
    controle = 123



    #função
    def gerar_quadrado(matriz):
        soma_linha1=(matriz[0][0] +matriz[0][1] +matriz[0][2])
        soma_linha2=(matriz[1][0]+ matriz[1][1]+matriz[1][2])
        soma_linha3=(matriz[2][0]+matriz[2][1]+matriz[2][2])
        soma_coluna1=(matriz[0][0] +matriz[1][0] +matriz[2][0])
        soma_coluna2=(matriz[0][1] +matriz[1][1] +matriz[2][1])
        soma_coluna3=(matriz[0][2] +matriz[1][2] +matriz[2][2])
        soma_diagonal1=(matriz[0][0] +matriz[1][1] +matriz[2][2])
        soma_diagonal2= (matriz[0][2] +matriz[1][1] +matriz[2][0])
        if ((soma_linha1 == soma_linha3)&(soma_linha2==soma_diagonal1)&
                ((soma_coluna1==soma_coluna2)&(soma_coluna3==soma_coluna1))&
                (soma_diagonal2==soma_diagonal1)):
            print("+---------+")
            print(f"|{matriz[0][0],matriz[0][1],matriz[0][2]}|\n"
                  f"|{matriz[1][0], matriz[1][1],matriz[1][2]}|\n"
                  f"|{matriz[2][0],matriz[2][1],matriz[2][2]}|")
            print("+---------+")
            return -123
        else:
            return 123
    #fimFunção
    tentativas = 1
    while controle != -123 :
        print(f'Tentativa nº{tentativas}')
        sequencia = [1,2,3,4,5,6,7,8,9]
        for i in range(3):
            for i1 in range(3):
                aux_numero = random.choice(sequencia)
                matriz[i][i1] = aux_numero
                aux_numero2 = sequencia.index(aux_numero)
                sequencia = sequencia[:aux_numero2] + sequencia[aux_numero2 + 1:]
        controle = gerar_quadrado(matriz)
        tentativas+=1



if __name__ == "__main__":
    ex01()
    ex02()
    ex03()
    ex04()
    ex05()
    ex06()
    ex07()
    ex08()
    ex09()
    ex10()
    ex11()
    ex12()
    ex13()
    ex14()

