# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'

def caesb():
    """A partir do dia 1º de junho de 2020, a Caesb começou a aplicar as alterações provocadas pela Lei Distrital no. 6.272/2019, que implementa uma nova forma de cobrança das tarifas de água e esgoto no Distrito Federal.
    A principal mudança na nova tabela é o fim do consumo mínimo de 10m³ por mês, relativos ao fornecimento de água de cada unidade de consumo. Com o fim da cobrança do consumo mínimo, o pagamento se dará pelo consumo efetivamente medido. Passa a ser arrecadada uma tarifa fixa de R$ 8,00 para a categoria residencial e R$ 21,00 para não residencial, sendo que a parte fixa será cobrada tanto para o serviço de água como para o serviço de esgoto, além da cobrança do valor efetivamente consumido de água e de esgoto.
    Hoje, 40% da população do DF estão na faixa de consumo de até 7 m³/mês e terão redução de até 65% na sua fatura. Um dos principais objetivos dessa mudança é estimular um uso mais racional e consciente de água, premissa presente em toda a nova estrutura. Ou seja, quem economizar mais, pagará menos.
    Tarifa Social - A principal inovação é a ampliação substancial do número de beneficiados pela tarifa social, de 3 mil para aproximadamente 70 mil famílias, com renda per capita entre zero e R$ 178,00. Para ter direito à Tarifa Social, basta que o titular da conta de água seja beneficiário do Programa Bolsa Família e esteja com CPF cadastrado e atualizado no CadÚnico pela SEDES.

    Tabela de Tarifa Mensal para o Período de 01/06/2020 a 31/05/2021
    (Tarifa Alterada pela Resolução Adasa Nº 12/2019, de 29 de Novembro de 2019)
Faixa m³            Vol. Faixa     Alíquota (R$) Preço p/ m³       Da Faixa (R$)
RESIDENCIAL PADRÃO                                                    8,00
0 a 7                  7                    2,99                      20,93
8 a 13                 6                    3,59                      21,54
14 a 20                7                    7,10                      49,70
21 a 30                10                   10,66                    106,60
31 a 45                15                   17,05                     255,75
Acima de 45                                 23,87
-
-------------------------------------------------------------------------------
Faixa m³            Vol. Faixa      Alíquota Preço p/ m³       Da Faixa (R$)
RESIDENCIAL SOCIAL                                                4,00

0 a 7                   7                 2,99                   20,93
8 a 13                  6                 3,59                   21,54
14 a 20                 7                 7,10                   49,70
21 a 30                 10               10,66                  106,60
31 a 45                 15               17,05                  255,75
Acima de 45             -                 23,87                     -

----------------------------------------------------------------------------


Faixa m³                 Vol. Faixa    Alíquota Preço p/ m³  Da Faixa (R$)
COMERCIAL, INDUSTRIAL E                                         R$ 21,00
PÚBLICA
0 a 4                         4                  6,14          R$ 24,56
5 a 7                         3                  7,68          R$ 23,04
8 a 10                        3                  9,98          R$ 29,94
11 a 40                      30                  12,48        R$ 37,440
Acima de 40                   -                  14,97              -

------------------------------------------------------------------------

Faixa m³           Vol. Faixa     Alíquota (R$)Preço p/ m³          Da Faixa (R$)
TARIFA PAISAGISMO                                                       31,50
0 a 4                   4                 9,21                          36,84
5 a 7                   3                 11,52                         34,56
8 a 10                  3                 14,97                         44,91
11 a 40                 30                18,72                         561,60
Acima de 40              -                22,46                           -
__________________________________________________________________________________
 Cálculo da Conta de Água:

No corpo da conta de água encontram-se discriminados os cálculos da tarifa fixa e da tarifa variável de água e esgotos, assim como de todos os demais serviços faturados.

A seguir apresentamos os procedimentos para o cálculo da conta de água.
1 - Residencial
Esclarecimento Inicial: Sabendo-se que, quanto maior o consumo de água, maior a tarifa praticada, como proceder nos casos em que várias residências são atendidas por uma única ligação de água?
Para estes casos é aplicado o conceito de unidade de consumo. Para exemplificar podemos citar um prédio residencial, onde cada apartamento corresponde a uma unidade de consumo. Este procedimento é adotado apenas para a categoria residencial.
Conforme adequações da Resolução ADASA nº 14/2011:
1º Passo:     dividir o consumo medido pelo número de unidades de consumo;
2º Passo:     distribuir nas faixas de consumo da tabela de tarifas definidas em Resolução da ADASA, o resultado do inciso anterior, observando o enquadramento na Tarifa Padrão ou Social;
3º Passo:     multiplicar o resultado da distribuição dos consumos, conforme inciso anterior pelo valor da parte variável da tarifa correspondente da faixa de consumo, observando a categoria e a classe da unidade usuária;
4º Passo:     somar os resultados obtidos no cálculo anterior ao valor da parte fixa da tarifa por unidade consumo, observando a categoria e a classe da unidade usuária;
5º Passo:     multiplicar os resultados obtidos no cálculo anterior pelo número de unidades de consumo, obtendo o valor da fatura de água;
6º Passo     valor da tarifa de esgotos corresponde a 100% do valor da tarifa de água, exceto quando se tratar de coleta do tipo condominial, que equivale a 60% do valor da tarifa de água.

2 - Categoria Não Residencial
Conforme adequações da Resolução ADASA nº 14/2011:
1º Passo:     distribuir o resultado do consumo medido nas faixas de consumo da tabela de tarifas;
2º Passo:     multiplicar o resultado da distribuição dos consumos obtidos no item anterior pelo valor da parte variável da tarifa correspondente da faixa de consumo;
3º Passo:     somar os resultados obtidos no cálculo anterior ao valor da parte fixa da tarifa, por unidade de consumo, obtendo o valor do serviço de abastecimento de água.



Observação:

A tarifa de esgotos será calculada com base na cobrança de água, podendo ser cobrados valores correspondentes a 50%, 60 ou 100% do valor da água, sendo o percentual definido de acordo com o sistema de esgotos utilizado no imóvel, conforme abaixo:

    Imoveis em obra: 50%;
    Sistema convencional: 100%
    Sistema Condominial: 60 ou 100% (dependendo da localização do ramal predial).

"""
#Definindo parametros
    taxa_egoto_dict={1:1,2:0.5,3:0.6}
    tipo_consumidor_dict = {1:'residencial',2:'não residencial'}
    tipo_tarifa_dict = {1:'residencial tipo padrão',2:'residencial tipo social'}
    tipo_consumidor = int(input(f"Informe o tipo de tarifa a ser calculada:\n1 - Residencial\n2 - Não residencial\nRESPOSTA: "))
    while (tipo_consumidor < 1) or (tipo_consumidor > 2):
        print(f'[{tipo_consumidor}] é uma opção inválida!!!!!')
        tipo_consumidor = int(input(f"Informe o tipo de tarifa a ser calculada: \n1 - Residencial\n2 - Não residencial\nRESPOSTA: "))
    consumo_m3 = int(input(f"Informe o seu consumo em m³:"))
    sistema_esgoto = int(input(f"Informe a atual situação do seu sitema e esgoto:\n1 - Convencional\n2 - Obra\n3 - Sistema Condominial\nRESPOSTA: "))
    while (sistema_esgoto <1) or (sistema_esgoto >3):
        print(f'[{sistema_esgoto}] é uma opção inválida!!!!!')
        sistema_esgoto = int(input(f"Informe a atual situação do seu sitema e esgoto:\n1 - Convencional\n2 - Obra\n3 - Sistema Condominial\nRESPOSTA:"))



#função
    def get_preco_agua(tipo_consumidor:int , consumo_m3:int ):
        tarifa_residencia_padrao = 8
        tarifa_residencia_social = 4
        tarifa_ind_com_pub = 21
        tarifa_paisagismo = 31.5
        preco_agua:float = 0

#residencial
        if tipo_consumidor == 1:
            tipo_tarifa = int(input(f"Informe a categoria da sua unidade consumidora {tipo_consumidor_dict.get(tipo_consumidor)}:\n1 - Padrão\n2 - Social:\nRESPOSTA:"))
            while (tipo_tarifa < 1) or (tipo_tarifa > 2):
                print(f'[{tipo_tarifa}] é uma opção inválida!!!!!')
                tipo_tarifa = int(input(f"Informe a categoria da sua unidade consumidora {tipo_consumidor_dict.get(tipo_consumidor)}:\n1 - Padrão\n2 - Social:\nRESPOSTA:"))
            unidades = int(input(f"Informe quantas unidades de consumo"))
            consumo_unidade = consumo_m3 / unidades

    #residencia padrão
            if tipo_tarifa == 1:

                if consumo_unidade  <= 7:
                    preco_agua = (2.99* consumo_unidade + tarifa_residencia_padrao)*unidades
                elif (consumo_unidade  >7)&(consumo_unidade  <=13):
                    preco_agua =( 3.59 * consumo_unidade + tarifa_residencia_padrao)*unidades
                elif (consumo_unidade  > 13) & (consumo_unidade  <= 20):
                    preco_agua = (7.1 * consumo_unidade + tarifa_residencia_padrao)*unidades
                elif (consumo_unidade  > 20) & (consumo_unidade  <= 30):
                    preco_agua = (10.66 * consumo_unidade + tarifa_residencia_padrao)*unidades
                elif (consumo_unidade  > 30) & (consumo_unidade  <= 45):
                   preco_agua =  (17.05 * consumo_unidade + tarifa_residencia_padrao)*unidades
                else:
                    preco_agua = (23.87 * consumo_unidade + tarifa_residencia_padrao)*unidades
    #residencia social
            else:
                if consumo_unidade  <= 7:
                    preco_agua = (1.49* consumo_unidade +tarifa_residencia_social)*unidades
                elif (consumo_unidade  >7)&(consumo_unidade  <=13):
                    preco_agua = (1.79 * consumo_unidade  +tarifa_residencia_social)*unidades
                elif (consumo_unidade  > 13) & (consumo_unidade  <= 20):
                    preco_agua = (3.55 * consumo_unidade +tarifa_residencia_social)*unidades
                elif (consumo_unidade  > 20) & (consumo_unidade  <= 30):
                    preco_agua = (5.33 * consumo_unidade +tarifa_residencia_social)*unidades
                elif (consumo_unidade  > 30) & (consumo_unidade   <= 45):
                    preco_agua = (17.05 * consumo_unidade +tarifa_residencia_social)*unidades
                else:
                    preco_agua = (23.87 * consumo_unidade +tarifa_residencia_social)*unidades
            return preco_agua
#não residencial
        else:
            tipo_tarifa = int(input(
                f"Informe a categoria da sua unidade consumidora: {tipo_consumidor_dict.get(tipo_consumidor)}:\n1 - Comercial, Industrial ou Pública\n2 - Paisagismo:\nRESPOSTA:"))
            while (tipo_tarifa < 1) or (tipo_tarifa > 2):
                print(f'[{tipo_tarifa}] é uma opção inválida!!!!!')
                tipo_tarifa = int(input(f"Informe a categoria da sua unidade consumidora {tipo_consumidor_dict.get(tipo_consumidor)}:\n1 - Comercial, Industrial ou Pública\n2 - Paisagismo:\nRESPOSTA:"))
                unidades = int(input(f"Informe quantas unidades de consumo"))

    #Comercial ,industrial ou público
            if tipo_tarifa == 1:
                if consumo_m3  <= 4:
                    preco_agua = 6.14 * consumo_m3
                elif (consumo_m3  > 4) & (consumo_m3  <= 7):
                    preco_agua = 7.68 * consumo_m3 +tarifa_ind_com_pub
                elif (consumo_m3  > 7) & (consumo_m3  <= 10):
                    preco_agua = 9.98 * consumo_m3 +tarifa_ind_com_pub
                elif (consumo_m3  > 10) & (consumo_m3  <= 40):
                    preco_agua = 12.48 * consumo_m3 +tarifa_ind_com_pub
                else:
                    preco_agua = 14.97 * consumo_m3 +tarifa_ind_com_pub
    # paisagismo
            else:
                if consumo_m3  <= 4:
                    preco_agua = 9.21 * consumo_m3  +tarifa_paisagismo
                elif (consumo_m3  > 4) & (consumo_m3  <= 7):
                    preco_agua = 11.52 * consumo_m3 +tarifa_paisagismo
                elif (consumo_m3  > 7) & (consumo_m3  <= 10):
                    preco_agua = 14.97 * consumo_m3 +tarifa_paisagismo
                elif (consumo_m3  > 10) & (consumo_m3  <= 40):
                    preco_agua = 18.72 * consumo_m3 +tarifa_paisagismo
                else:
                    preco_agua = 22.46 * consumo_m3 +tarifa_paisagismo

            return preco_agua


    preco_agua = get_preco_agua(tipo_consumidor,consumo_m3)
    preco_esgoto = preco_agua * taxa_egoto_dict.get(sistema_esgoto)
    preco_total = preco_agua+preco_agua
    print("----------------------------------------")
    print(f"Conta da unidade:\nAgua:           {preco_agua:.2f}\nEsgoto:         {preco_esgoto:.2f}\nPreco Total:    {preco_total:.2f}")
    print("----------------------------------------")










if __name__ == "__main__":
    caesb()