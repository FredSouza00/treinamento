# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'

def ex01():
#Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem caso o valor seja inválido e continue pedindo até que o usuário informe um valor válido.
    nota: int = int(input('Insira uma nota inteira de valor entre 1 e 10: '))
    if (nota >= 0) & (nota <= 10):
        print("A nota é válida!")
        return
    else:
        while(nota < 0)or(nota > 10):
            print('A nota é inválida. Digite outra nota de 1 a 10: ')
            nota = int(input('Insira uma nota inteira de valor entre 1 e 10: '))
    print("A nota é válida!")

def ex02():
#Faça um programa que leia um nome de usuário e a sua senha e não aceite a senha igual ao nome do usuário, mostrando uma mensagem de erro e voltando a pedir as informações.
    login = input('Insira seu nome de usuário: ')
    password: str = input('Insira sua senha: ')
    if login == password:
        print('ERRO!!! login e senha não podem ser identicos.')
        while login == password:
            login = input('Insira novo nome de usuário: ')
            password = input('Insira nova senha: ')
        print("Conta criada com sucesso!!")
    else:
        print("Conta criada com sucesso!!")

def ex03():
#Faça um programa que leia e valide as seguintes informações:
#Nome: maior que 3 caracteres;
#Idade: entre 0 e 150;
#Salário: maior que zero;
#Sexo: 'f' ou 'm';
#Estado Civil: 's', 'c', 'v', 'd';
    nome = input('Insira seu nome: ')
    if (len(nome) < 4):
        while (len(nome) < 4):
            print('Insira nome com mais de 3 caracteres: ')
            nome = input('Insira seu nome novamente: ')
    idade = int(input('Digite sua idade: '))
    if (idade<0) or (idade>150):
        while(idade<0) or (idade>150):
            print('ERRO!!! Idade minima de 0 anos e maxima de 150 anos: ')
            idade = int(input('Digite sua idade novamente: '))
    salario = float(input('Informe seu salário: '))
    if salario <= 0:
        while salario <= 0:
            print("ERRO!!! O salário precisa ser superior a 0")
            salario = float(input('Informe seu salário novamente: '))
    sexo = input('Qual seu sexo (F / M): ')
    if (sexo != "F") & (sexo != "f") & (sexo != "M") & (sexo != "m"):
        while(sexo != "F") & (sexo != "f") & (sexo != "M") & (sexo != "m"):
            print("ERRO!!! O sexo deve ser representado por F para feminino ou M para masculino")
            sexo = input('informe seu sexo novamente (F / M): ')
    estado_civil = input('Estado civil (s, c, v ou d): ')
    if(estado_civil != 's') and (estado_civil != 'c') and (estado_civil != 'v') and (estado_civil != 'd'):
        while(estado_civil != 's') and (estado_civil != 'c') and (estado_civil != 'v') and (estado_civil != 'd'):
            print("ERRO!!!As opções válidas são :'s' para (solteiro), 'c' para (casado),'v' para (viuvo) ou 'd' para (Divorciado)):")
            estado_civil = input('Informe Estado civil novamente: (s, c, v ou d): ')
    print('dados validados com sucesso!!!')

def ex04():
#Supondo que a população de um país A seja da ordem de 80000 habitantes com uma taxa anual de crescimento de 3% e que a população de B seja 200000 habitantes com uma taxa de crescimento de 1.5%. Faça um programa que calcule e escreva o número de anos necessários para que a população do país A ultrapasse ou iguale a população do país B, mantidas as taxas de crescimento.
    popA = 80000
    popB = 200000
    ano = 0
    while popA <= popB:
        popA += (popA * 0.03)
        popB += (popB* 0.015)
        ano += 1
    print(f'Em simulação  a População do país A ultrapassou/ igualou sua população em {ano} anos')

def ex05():
#Copie o programa anterior e altere permitindo ao usuário informar as populações e as taxas de crescimento iniciais. Valide a entrada e permita repetir a operação.
    print("infome os dados para criar a simulação comparativa de 2 populações")
    popA = float(input('informe a população total do país A : '))
    taxaPopA = float(input('Informe a taxa de crescimento anual da população do país A : '))
    popB = float(input('informe a população total do país B : '))
    taxaPopB = float(input('Informe a taxa de crescimento anual da população do país A : '))
    ano = 0
    paismenor =""
    if popA <= popB:
        while popA <= popB:
            paismenor = "A"
            paismaior = "B"
            popA += (popA * (taxaPopA/100))
            popB += (popB * (taxaPopB/100))
            ano += 1
    else:
        while popB <= popA:
            paismenor = "B"
            paismaior = "A"
            popA += (popA * (taxaPopA / 100))
            popB += (popB * (taxaPopB / 100))
            ano += 1
    print(f'Em simulação  a População do país {paismenor} ultrapassou/ igualou sua  ao {paismaior} em {ano} anos')

def ex06():
#Faça um programa que imprima na tela os números de 1 a 20, um abaixo do outro.
    numero=1
    while numero<=20:
        print(numero)
        numero +=1

def ex07():
#Copie o programa anterior, modifique para que ele mostre os números um ao lado do outro
    print(list(range(21)))

def ex08():
# 08Faça um programa que leia 5 números e informe o maior número.
    listaNumeros = []
    for num in range(1, 6):
        listaNumeros.append(int(input(f'Digite um número {num}: ')))

    print(f'Você digitou os seguintes números: {listaNumeros} e o maior número é {max(listaNumeros)}')

def ex09():
#Faça um programa que leia 5 números e informe a soma e a média dos números.
    listaNumeros = []
    for num in range(1, 6):
        listaNumeros.append(int(input(f'Digite um número {num}: ')))
    print(f'A soma dos numeros digitados é : {sum(listaNumeros)} e a média entre eles é de: {(sum(listaNumeros))/len(listaNumeros)}')

def ex10():
#Faça um programa que imprima na tela apenas os números ímpares entre 1 e 50.
    print(list(range(1, 51, 2)))

def ex11():
#Faça um programa que receba dois números inteiros e gere os números inteiros que estão no intervalo compreendido por eles.
    numero1 = int(input('Insira o 1º número: '))
    numero2 = int(input('Insira o 2º número: '))
    for numX in range(numero1, numero2 + 1):
        print(numX)

def ex12():
#Copie e altere o programa anterior para mostrar no final a soma dos números.
    numero1 = int(input('Insira o 1º número: '))
    numero2 = int(input('Insira o 2º número: '))
    soma = 0
    for numX in range(numero1, numero2 + 1):
        print(numX)
        soma += numX
    print(f'A soma da iteração é {soma}')
def ex13():
#Desenvolva um gerador de tabuada
    numero = int(input('Insira um número entre 1 e 10 e veja sua tabuada: '))
    index = 1
    while index <= 10:
        resposta = numero * index
        print(f'[{numero}] x [{index}] = [{resposta}]')
        index += 1
def ex14():
#Faça um programa que peça dois números, base e expoente, calcule e mostre o primeiro número elevado ao segundo número. Não utilize a função de potência da linguagem.
    base = int(input('Insira o número base: '))
    expoente = int(input('Insira o número expoente: '))
    potencia = 1
    for numX in range(expoente):
        potencia = potencia * base
        numX = numX + 1
    print(potencia)

def ex15():
#Faça um programa que peça 10 números inteiros, calcule e mostre a quantidade de números pares e a quantidade de números ímpares.
    listaNumeros = []
    for index in range(1, 11):
        listaNumeros.append(int(input(f'Insira{index}º numero: ')))

    par = 0
    impar = 0
    for index in listaNumeros:
        if index % 2 == 0:
            par = par + 1
        else:
            impar = impar + 1

    print(f"{listaNumeros}\nSão {par} numeros pares e {impar} numeros impares.")
def ex16():
#A série de Fibonacci é formada pela seqüência 1,1,2,3,5,8,13,21,34,55,... Faça um programa capaz de gerar a série até o n−ésimo termo.
    terceiro= 1
    segundo= 1
    index = 1
    print(f'{terceiro}\n{segundo}')
    while index <= 9:
        aux = terceiro + segundo
        segundo = terceiro
        terceiro = aux
        index += 1
        print(aux)


ex01()
ex02()
ex03()
ex04()
ex05()
ex06()
ex07()
ex08()
ex09()
ex10()
ex11()
ex12()
ex13()
ex14()
ex15()
ex16()