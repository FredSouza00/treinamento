# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'


def ex01():
    """Faça um Programa que leia um vetor de 5 números inteiros e mostre-os.
"""
    numero_inteiro = []
    for i in range(5):
        numero = int(input(f"informe o {i + 1}º numero inteio:"))
        numero_inteiro.append(numero)
    print(f"A lista de numeros é :{numero_inteiro}")


def ex02():
    """Faça um Programa que leia um vetor de 10 números reais e mostre-os na ordem inversa."""
    numero_real = []
    for i in range(10):
        numero = float(input(f"informe o {i + 1}º numero real:"))
        numero_real.append(numero)
    for i in numero_real[::-1]:
        print(f"A lista ao contrário é:{i}")


def ex03():
    """Faça um Programa que leia 4 notas, mostre as notas e a média na tela.
"""
    notas = []
    for i in range(4):
        numero = float(input(f"informe a {i + 1}ºnota :"))
        notas.append(numero)

    print(f"A média das notas é de:{sum(notas) / len(notas)}")


def ex04():
    """Faça um Programa que leia um vetor de 10 caracteres, e diga quantas consoantes foram lidas. Imprima as
    consoantes. """
    vogais_lista = []
    consoantes_lista = []
    vogais = 'aeiouAEIOU'
    for i in range(10):
        letra = input(f'Informe o {i + 1}º caracter: ')
        if letra in vogais:
            vogais_lista.append(letra)
            continue
        else:
            consoantes_lista.append(letra)

    print(f'Foram inseridas: {len(consoantes_lista)} consoantes,são elas: {consoantes_lista}!!!')


def ex05():
    """Faça um Programa que leia 20 números inteiros e armazene-os num vetor. Armazene os números pares no vetor PAR
    e os números IMPARES no vetor impar. Imprima os três vetores.
    """
    numeros = []
    numeros_pares = []
    numeros_impares = []
    for i in range(20):
        numero = int(input("Digite o valor: "))
        numeros.append(numero)
        if numero % 2 == 0:
            print(f'O número {numero} é par!')
            numeros_pares.append(numero)

        else:
            print(f'O número {numero} é ímpar!')
            numeros_impares.append(numero)

    print(f"Todos os numeros:{numeros}\nTodos os pares:{numeros_pares}\nTodos os impares{numeros_impares}")


def ex06():
    """Faça um Programa que peça as quatro notas de 10 alunos, calcule e armazene num vetor a média de cada aluno,
    imprima o número de alunos com média maior ou igual a 7.0.
"""

    notas_boas = []
    medias = []
    for i in range(10):
        notas = []
        for i2 in range(4):
            notas.append(float(input(f"Digite a {i2}ª nota do {i + 1}º aluno: ")))
        medias.append(sum(notas) / len(notas))
        if (sum(notas) / len(notas)) >= 7:
            notas_boas.append(sum(notas) / len(notas))
    print(f"O total de alunos que tiveram a nota igual ou superior a média foram {len(notas_boas)}")


def ex07():
    """Faça um Programa que leia um vetor de 5 números inteiros, mostre a soma, a multiplicação e os números.
"""
    numeros = []

    for i in range(0, 5):
        numeros.append(int(input(f'Informe o {i + 1}º número de 5: ')))
    somar = 0
    multiplicar = 1
    for i in numeros:
        somar += i
        multiplicar *= i
    print(f"Numeros: {numeros}\n Somas: {somar}\n Produtos: {multiplicar}")


def ex08():
    """"Faça um Programa que peça a idade e a altura de 5 pessoas, armazene cada informação no seu respectivo vetor.
Imprima a idade e a altura na ordem inversa a ordem lida."""
    altura_list = []
    idade_list = []
    for i in range(5):
        altura_list.append(float(input(f"Informe sua altura em metros: ")))
        idade_list.append(int(input(f"informe sua idade: ")))

    for i in altura_list[::-1]:
        print(f"A lista de altura ao contrário é: {i}")
    for i in idade_list[::-1]:
        print(f"A lista de idades ao contrário é: {i}")


def ex09():
    """Faça um Programa que leia um vetor A com 10 números inteiros, calcule e mostre a soma dos quadrados dos eleme
    ntos do vetor."""
    a = []
    quadrados = 0
    for i in range(10):
        a.append(int(input(f'informe o numero {i + 1} de 10 : ')))
    for i in a:
        quadrados += i * i
    print(f'A soma dos quadrados dos numeros informados é: {quadrados}')


def ex10():
    """Faça um Programa que leia dois vetores com 10 elementos cada. Gere um terceiro vetor de 20 elementos,
    cujos valores deverão ser compostos pelos elementos intercalados dos dois outros vetores.
    """
    vetor1 = []
    vetor2 = []
    vetor_global = []
    for i in range(10):
        vetor1.append(int(input(f"informe o {i + 1}º valor do vetor 1: ")))
        vetor2.append(int(input(f"informe o {i + 1}º valor do vetor 2: ")))
    for i in range(10):
        vetor_global.append(vetor1[i])
        vetor_global.append(vetor2[i])
    print(f"Resultado de vetores intercalados: {vetor_global}")


def ex11():
    """Altere o programa anterior, intercalando 3 vetores de 10 elementos cada."""
    vetor1 = []
    vetor2 = []
    vetor3 = []
    vetor_global = []
    for i in range(10):
        vetor1.append(int(input(f"informe o {i + 1}º valor do vetor 1: ")))
        vetor2.append(int(input(f"informe o {i + 1}º valor do vetor 2: ")))
        vetor3.append(int(input(f"informe o {i + 1}º valor do vetor 3: ")))
    for i in range(10):
        vetor_global.append(vetor1[i])
        vetor_global.append(vetor2[i])
        vetor_global.append(vetor3[i])
    print(f"Resultado de vetores intercalados: {vetor_global}")


def ex12():
    """Foram anotadas as idades e alturas de 30 alunos. Faça um Programa que determine quantos alunos com mais de 13
     anos possuem altura inferior à média de altura desses alunos"""
    altura_list = []
    idade_list = []

    sete_anoes: int = 0
    for i in range(30):
        altura_list.append(float(input(f"Informe a altura do aluno nª{i + 1} em metros: ")))
        idade_list.append(int(input(f"Informe a idade do aluno nª{i + 1} : ")))
    altura_media = sum(altura_list) / len(altura_list)
    for i in range(30):
        if (altura_list[i] < altura_media) & (idade_list[i] > 13):
            sete_anoes += 1
    print(f"A quantidade de alunos com idade superior a 13 anos e altura inferior a média geral é de {sete_anoes}")


def ex13():
    """Faça um programa que receba a temperatura média de cada mês do ano e armazene-as em uma lista. Após isto,
    calcule a média anual das temperaturas e mostre todas as temperaturas acima da média anual, e em que mês elas
    ocorreram (mostrar o mês por extenso: 1 – Janeiro, 2 – Fevereiro, . . . ).
"""
    meses_temp = []
    meses_dict = {1: "janeiro", 2: "fevereiro", 3: "março", 4: "abril", 5: "mail", 6: "junio", 7: "julio", 8: "agosto",
                  9: "setembro",
                  10: "outubro", 11: "novembro", 12: "dezembro"}
    meses_atipicos = []
    for i in range(12):
        meses_temp.append(float(input(f"Informa a temperatura em ºC do mes de {meses_dict.get(i + 1)}:")))
    media_anual = sum(meses_temp) / len(meses_temp)
    for i in range(12):
        if meses_temp[i] > (sum(meses_temp) / len(meses_temp)):
            meses_atipicos.append(i + 1)
    print(f"A temperatúra média anual foi de: {media_anual:.2f}ºC\n Os meses atípicos foram: ")
    for i in range(len(meses_atipicos)):
        print(f"{meses_atipicos[i]}-{meses_dict.get(meses_atipicos[i])}")


def ex14():
    """Utilizando listas faça um programa que faça 5 perguntas para uma pessoa sobre um crime. As perguntas são:
    "Telefonou para a vítima?" "Esteve no local do crime?" "Mora perto da vítima?" "Devia para a vítima?" "Já
    trabalhou com a vítima?" O programa deve no final emitir uma classificação sobre a participação da pessoa no
    crime. Se a pessoa responder positivamente a 2 questões ela deve ser classificada como "Suspeita", entre 3 e 4
    como "Cúmplice" e 5 como "Assassino". Caso contrário, ele será classificado como "Inocente".
    """
    perguntas = ["Telefonou para a vítima?", "Esteve no local do crime?", "Mora perto da vítima?",
                 "Devia para a vítima?", "Já trabalhou com a vítima?"]
    sentenca_dict = {0: "inocente", 1: "inocente", 2: "suspeita", 3: "cumplice", 4: "cumplice", 5: "assasino(a)"}
    indice_de_culpabilidade = 1

    for i in range(len(perguntas)):
        resposta = input(f"Responda Sim/Não :  \n{perguntas[i]}")
        if resposta.lower() == "sim":
            indice_de_culpabilidade += 1
    print(f"Considerado: {sentenca_dict.get(indice_de_culpabilidade)}")


def ex15():
    """Faça um programa que leia um número indeterminado de valores, correspondentes a notas, encerrando a entrada de
    dados quando for informado um valor igual a -1 (que não deve ser armazenado). Após esta entrada de dados,
    faça: Mostre a quantidade de valores que foram lidos; Exiba todos os valores na ordem em que foram informados,
    um ao lado do outro; Exiba todos os valores na ordem inversa à que foram informados, um abaixo do outro; Calcule
    e mostre a soma dos valores; Calcule e mostre a média dos valores; Calcule e mostre a quantidade de valores acima
    da média calculada; Calcule e mostre a quantidade de valores abaixo de sete; Encerre o programa com uma mensagem;
    """

    valores = []
    controle = 0
    frequencia = 0
    menor_sete = 0
    while controle != -1:
        controle = float(input(f"informe uma nota"))
        if controle != -1:
            valores.append(controle)
    media = sum(valores) / len(valores)
    for i in valores:
        if i > media:
            frequencia += 1
        if i < 7:
            menor_sete += 1
    print(f"A quantidade de valores é de {len(valores)}")
    print(f"Impressos lado a lado {valores}")
    print(f"impressos um abaixo do outro em ordem inversa")
    for i in valores[::-1]:
        print(i)

    print(f"Soma dos valores: {sum(valores)}")
    print(f"Média dos valores:{media}")
    print(f"Quantidade de valores acima da média: {frequencia}")
    print(f"Frequencia de notas menores que sete: {menor_sete}")
    print("Essa mensagem encerra o programa\n XAUUU BRIGADU")


if __name__ == "__main__":
    ex01()
    ex02()
    ex03()
    ex04()
    ex05()
    ex06()
    ex07()
    ex08()
    ex09()
    ex10()
    ex11()
    ex12()
    ex13()
    ex14()
    ex15()
