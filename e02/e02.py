# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@FredericoSouza'
from datetime import date, datetime
import random
import re

def ex01():
#Tamanho de strings. Faça um programa que leia 2 strings e informe o conteúdo delas seguido do seu comprimento. Informe também se as duas strings possuem o mesmo comprimento e são iguais ou diferentes no conteúdo
    str1 = input('Digite uma String: ')
    str2 = input('Digite outra String: ')
    str1len = len(str1)
    str2len = len(str2)
    print(f'{str1}: {str1len} caracteres')
    print(f'{str2}: {str2len} caracteres')

    if str1len == str2len:
        print("Ambas possuem tamanhos iguais")
    else:
        print("Ambas possuem tamanhos diferentes")
    if str1 == str2:
        print("Ambas possuem caracteres iguais")
    else:
        print("Ambas possuem caracteres diferentes")
    print("----------------------------------------------------------")



def ex02():
#Nome ao contrário em maiúsculas.
    nome = input("Digite um nome para imprimir ao contrario em letras maiúsculas: ")
    print((nome[::-1]).upper())
    print("----------------------------------------------------------")
def ex03():
#03Nome na vertical. Faça um programa que solicite o nome do usuário e imprima-o na vertical.
    nome = input("Digite um nome para imprimir verticalmente : ")
    i = 0
    while i < len(nome):
        print(nome[i])
        i += 1
    print("----------------------------------------------------------")

def ex04():
#04 Nome na vertical em escada. Modifique o programa anterior de forma a mostrar o nome em formato de escada.

    nome = input("Digite um nome para imprimir em efeito de escadaria: ")
    i = 1
    while i <= len(nome):
        print(nome[:i])
        i += 1
    print("----------------------------------------------------------")

def ex05():
#Nome na vertical em escada invertida. Altere o programa anterior de modo que a escada seja invertida.
    nome = input("Digite um nome para imprimir em escada invertida: ")
    i = 0
    while i < len(nome):
        print(nome[i:])
        i += 1
    print("----------------------------------------------------------")


def ex06():
#Data por extenso. Faça um programa que solicite a data de nascimento (dd/mm/aaaa) do usuário e imprima a data com o nome do mês por extenso


    data = input(("Informe sua data de nascimento seguindo o formato: (dd/mm/aaaa) com '/': "))
    print(f'Sua data de nascimento é {datetime.strptime(data, "%d/%m/%Y").strftime("%d de %B de %Y.")}')
    print("----------------------------------------------------------")
def ex07():
#Conta espaços e vogais. Dado uma string com uma frase informada pelo usuário (incluindo espaços em branco), conte:
    sentenca = input("Digite uma frase para ser analisada: ")
    blankSpace = 0
    vog = 0

    for letra in sentenca:
        if letra in "aeiouAEIOU":
            vog += 1
        elif letra == " ":
            blankSpace += 1
    print(f'A frase,{sentenca}, tem {vog} vogais e {blankSpace} espaços em branco.')
    print("----------------------------------------------------------")

def ex08():
#Palíndromo. Um palíndromo é uma seqüência de caracteres cuja leitura é idêntica se feita da direita para esquerda ou vice−versa.
    testada = input("Insira uma palavra ou frase e descubra se é um palíndromo: ")
    testadaSemEspacos = testada.replace(' ', '')
    testadaReversa = testadaSemEspacos[::-1]
    if testadaSemEspacos == testadaReversa:
        print(f'A frase [{testada}] é palíndromo')
    else:
        print(f'A frase [{testada}] Não é palíndromo')
        print("----------------------------------------------------------")
    print("----------------------------------------------------------")
def ex09():
#Verificação de CPF. Desenvolva um programa que solicite a digitação de um número de CPF no formato xxx.xxx.xxx-xx e indique se é um número válido ou inválido através da validação dos dígitos verificadores e dos caracteres de formatação.
    numeros = {0:'0123456789',1:'0123456789',2:'0123456789',3:'.',4:'0123456789',5:'0123456789'
,6:'0123456789',7:'.',8:'0123456789',9:'0123456789',10:'0123456789',11:'-',12:'0123456789',13:'0123456789'}
    cpf = input(f"Informe seu cpf no formato (xxx.xxx.xxx-xx) :")

    for i in range(14):
        if cpf[i] in numeros.get(i):
            print(f'Caractere nº{i+1}:[{cpf[i]} válido!!!]')
        else:
            print(f'Formato inválido presente no caracter nª{i+1}, [{cpf[i]}] deveria conter:[{numeros.get(i)}]')
            return
    print(f'{cpf} está em formato válido.')


def ex10():
#Escreva um programa que solicite ao usuário a digitação de um número até 99 e imprima-o na tela por extenso.
    numero = int(input('Insira um número entre 0 e 99: '))
    if (numero >= 0) & (numero <= 99):
        numeroTransformado = stringfy(numero)
        print(f'Você digitou o número {numeroTransformado}.')
    else:
        print("Número inválido.")

    print("----------------------------------------------------------")

def stringfy(numeroTestado):
    numerosConjunto = {
        0: 'zero', 1: 'um', 2: 'dois', 3: 'três', 4: 'quatro', 5: 'cinco',
        6: 'seis', 7: 'sete', 8: 'oito', 9: 'nove', 10: 'dez',
        11: 'onze', 12: 'doze', 13: 'treze', 14: 'quatorze', 15: 'quinze',
        16: 'dezesseis', 17: 'dezessete', 18: 'dezoito', 19: 'dezenove', 20: 'vinte',
        30: 'trinta', 40: 'quarenta', 50: 'cinquenta', 60: 'sessenta', 70: 'setenta',
        80: 'oitenta', 90: 'noventa', }

    if numeroTestado % 10 == 0 or numeroTestado < 20:
        return numerosConjunto.get(numeroTestado)
    decimal = numeroTestado // 10 * 10
    unidade = numeroTestado % 10
    extenso = f'{numerosConjunto.get(decimal)} e {numerosConjunto.get(unidade)}'
    return extenso

def ex11():
#Jogo de Forca. Desenvolva um jogo da forca. O programa terá uma lista de palavras lidas de um arquivo texto e escolherá uma aleatoriamente. O jogador poderá errar 6 vezes antes de ser enforcado.
    arquivo = open('../resource/palavras.txt', 'r')
    palavras = arquivo.read().split()
    arquivo.close()
    palavra_jogo = random.choice(palavras).lower()
    tracos_forca = ('-'*len(palavra_jogo))
    letras_certas = ''
    chances = (6)


    def forca(tentativa):
        nonlocal letras_certas
        nonlocal tracos_forca
        if tentativa.lower() in palavra_jogo:
            tracos_forca = re.sub(f'[^{tentativa}\|\^{letras_certas}]', '-', palavra_jogo)
            letras_certas = letras_certas + tentativa
            print(f'A letra {tentativa} está na palavra!!!')
            return 0
        else:
            print(f'A letra {tentativa} não está na palavra!!!')
            return -1

    while chances != 0:
        print(f'---------Forca--------------\n'
              f'Voce tem {chances} tentativas.:\n'
              f'___________\n'
              f'||        |\n'
              f'||        O\n'
              f'||       /||\n'
              f'||       /| \n'
              f'||           \n'
              f'||====================\n'
              f'( palavra:{tracos_forca}  DICA: {len(palavra_jogo)} letras.')
        tentativa = input(f'Qual letra acha que tem nesta palavra?')
        chances += forca(tentativa)

        if palavra_jogo == tracos_forca:
            print("Parabens vc ganhou!!!")
            return

    print("Você foi inforcado!!!")

def ex12():
#Valida e corrige número de telefone. Faça um programa que leia um número de telefone, e corrija o número no caso deste conter somente 7 dígitos, acrescentando o '3' na frente. O usuário pode informar o número com ou sem o traço separador.
    telefone = 0
    telefone = input(f"Informe seu numero de telefone fixo:")
    novo_telefone = []
    try:

        if (len(telefone) < 7)or(len(telefone)>= 10):
            raise ValueError
        elif (len(telefone)== 9) & (telefone[4]!='-'):
            raise ValueError
        elif (len(telefone) == 9) & (telefone[4] == '-'):
            print(f'Seu telefone está correto: {telefone}')
        elif ('-' not in telefone ) &(len(telefone) == 8):
            for i in range(8):
                if (i == 4)&('-' not in telefone):
                    print(f"Aparentemente o numero: {telefone} precisa ser formatado com o '-' ")
                    novo_telefone.append('-')
                    novo_telefone.append(telefone[i])
                    continue
                novo_telefone.append(telefone[i])
            str
            string2 = str(novo_telefone).strip('[]')

            print(f'Seu telefone formatado é: {string2}')
            return
        elif(len(telefone)== 8) & (telefone[3] == '-'):
            for i in range(8):
                if i == 0:
                    print(f"Aparentemente o numero: {telefone} precisa ser formatado com o 3  ")
                    novo_telefone.append('3')
                    novo_telefone.append(telefone[i])
                    continue

                novo_telefone.append(telefone[i])
            str
            string2 = str(novo_telefone).strip('[]')

            print(f'Seu telefone formatado é: {string2}')
            return
        elif (len(telefone) == 7) & ('-' not in telefone):
            for i in range(7):
                if i == 0:
                    print(f"Aparentemente o numero: {telefone} precisa ser formatado com o 3  ")
                    novo_telefone.append('3')
                    novo_telefone.append(telefone[i])
                    continue
                if i == 3:
                    print(f"Aparentemente o numero: {telefone} precisa ser formatado com o '-' ")
                    novo_telefone.append('-')
                    novo_telefone.append(telefone[i])
                    continue
                novo_telefone.append(telefone[i])
            str
            string2 = str(novo_telefone).strip('[]')

            print(f'Seu telefone formatado é: {string2}')
            return



    except ValueError:
        if len(telefone) == 0:
            print("Você não digitou o  numero")
            return

        else:
            print(f"Numero inválido o numero deve conter entre 7 e 8 numeros inteiros:\nSeu numero {telefone} ")
            return

def ex13():
#jogo da palavra embaralhada. Desenvolva um jogo em que o usuário tenha que adivinhar uma palavra que será mostrada com as letras embaralhadas. O programa terá uma lista de palavras lidas de um arquivo texto e escolherá uma aleatoriamente. O jogador terá seis tentativas para adivinhar a palavra. Ao final a palavra deve ser mostrada na tela, informando se o usuário ganhou ou perdeu o jogo.
    arquivo = open('../resource/palavras.txt', 'r')
    palavras = arquivo.read().split()
    arquivo.close()
    palavra_jogo = random.choice(palavras).lower()
    chances = (6)
    def embaralhar_palavra(palavra):
        array = list(palavra)
        random.shuffle(array)
        array = ''.join(array)
        return array
    palavra_x = embaralhar_palavra(palavra_jogo)
    tentativa =''
    while tentativa != 0:

        print(f'{chances} tentativas restantes:\n'
              f'adivinhe essa palavra embaralhada\n'
              f'-----------{palavra_x}-----------  ')
        tentativa = input("tente!!:")
        if palavra_jogo.lower() == tentativa.lower():
            print(f"Você acertou !!!\n"
                  f"a palavra é : {palavra_jogo}")
            return
        else:
            chances -=1
            print(f"Você errou!!{chances} restantes")

def ex14():
#Leet spek generator. Leet é uma forma de se escrever o alfabeto latino usando outros símbolos em lugar das letras, como números por exemplo
    #print("Ex14 não está pronto")

    def portugues_leek(string):
        for char in string:
            if char == 'a':
                string = string.replace('a', '4')
            elif char == 'b':
                string = string.replace('b', '8')
            elif char == 'c':
                string = string.replace('c', '<')
            elif char == 'd':
                string = string.replace('d', '|)')
            elif char == 'e':
                string = string.replace('e', '3')
            elif char == 'f':
                string = string.replace('f', '|=')
            elif char == 'g':
                string = string.replace('g', '6')
            elif char == 'h':
                string = string.replace('h', '#')
            elif char == 'i':
                string = string.replace('i', '|')
            elif char == 'j':
                string = string.replace('j', '_|')
            elif char == 'k':
                string = string.replace('k', ' |<')
            elif char == 'l':
                string = string.replace('l', '1_')
            elif char == 'm':
                string = string.replace('m', '44')
            elif char == 'n':
                string = string.replace('n', '|\|')
            elif char == 'o':
                string = string.replace('o', '0')
            elif char == 'p':
                string = string.replace('p', '|o')
            elif char == 'q':
                string = string.replace('q', 'O_')
            elif char == 'r':
                string = string.replace('r', ' |2')
            elif char == 's':
                string = string.replace('s', '5')
            elif char == 't':
                string = string.replace('t', '7')
            elif char == 'u':
                string = string.replace('u', '|_|')
            elif char == 'v':
                string = string.replace('v', '|/')
            elif char == 'x':
                string = string.replace('x', '%')
            elif char == 'y':
                string = string.replace('y', '¥')
            elif char == 'z':
                string = string.replace('z', ' 2')

            else:
                pass
        print(string)

    palavra = input("Informe seu nome e veja ele em leek speak: ")
    portugues_leek(palavra)


if __name__ == "__main__":
    ex01()
    ex02()
    ex03()
    ex04()
    ex05()
    ex06()
    ex07()
    ex08()
    ex09()
    ex10()
    ex11()
    ex12()
    ex13()
    ex14()